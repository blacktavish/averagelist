package com.demo.springboot.dto;

public class AverageDto {
    Double avg;

    public AverageDto(Double avg) {
        this.avg = avg;
    }

    public Double getAvg() {
        return avg;
    }
}
