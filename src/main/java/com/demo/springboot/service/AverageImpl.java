package com.demo.springboot.service;

import com.demo.springboot.dto.AverageDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AverageImpl implements Average{


    @Override
    public AverageDto Calculate(List<Double> digits) {
        Double average = 0.0;
        for(double digit: digits){
            average += digit;
            System.out.println(digit);
        }
        return new AverageDto(average/digits.size());
    }
}
